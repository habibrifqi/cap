<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaksi".
 *
 * @property int $id
 * @property string|null $nama_pemesan
 * @property int $total_beli
 * @property int $id_barang
 *
 * @property Barang $barang
 */
class Transaksi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_pemesan'], 'required'],
            [['nama_pemesan'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pemesan' => 'Nama Pemesan',
        ];
    }

    /**
     * Gets query for [[Barang]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getBarang()
    // {
    //     return $this->hasOne(Barang::className(), ['id_barang' => 'id_barang']);
    // }
}
