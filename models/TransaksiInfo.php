<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaksi_info".
 *
 * @property int $id
 * @property string $nama_menu
 * @property int $jumlah
 * @property int $sub_harga
 * @property int $id_tansaksi
 *
 * @property Transaksi $tansaksi
 */
class TransaksiInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_menu', 'jumlah', 'sub_harga',], 'required'],
            [['jumlah', 'sub_harga', 'id_tansaksi'], 'integer'],
            [['nama_menu'], 'string', 'max' => 120],
            // [['id_tansaksi'], 'exist', 'skipOnError' => true, 'targetClass' => Transaksi::className(), 'targetAttribute' => ['id_tansaksi' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_menu' => 'Nama Menu',
            'jumlah' => 'Jumlah',
            'sub_harga' => 'Sub Harga',
        ];
    }

    /**
     * Gets query for [[Tansaksi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTansaksi()
    {
        return $this->hasOne(Transaksi::className(), ['id' => 'id_tansaksi']);
    }
}
