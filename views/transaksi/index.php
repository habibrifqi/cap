<?php
/* @var $this yii\web\View */
// use yii\helpers\Html;
use kartik\select2\Select2Asset;
use yii\widgets\ActiveForm;
use app\controllers\TransaksiController;
use yii\helpers\ArrayHelper;
use app\models\Barang;
use kartik\select2\Select2;
use yii\bootstrap4\Html;


$this->title = 'Transaksi';
$this->params['breadcrumbs'][] = $this->title;

$session = Yii::$app->session;
$session->open();
$name = $session->get('name');
?>
<style>
    .cc{
        background-color: red;
        margin: 0;
        padding: 0;
        width: 100%;

    }
</style>
<div>
    <?php 

    ?>
</div>


<div class="cc">
    <div class="col-md-8 order-md-1">
            <div class="row">
                <div class="col-md-12 mb-3 box box-primary">
                <?php if(!$name) { ?>
                <?php $form = ActiveForm::begin(); ?>
                
                <?= $form->field($model1, 'nama_pemesan')->textInput(['maxlength' => true]) ?>

                <div class="box-footer">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                <?php }else{ ?>
                    <div class="box">
                    <?= Html::a('buat pesanan', ['test',], ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
                <?php $form = ActiveForm::begin(); ?>
                
                <?//= $form->field($model, 'nama_menu')->textInput(['maxlength' => true]) ?>


                <?= $form->field($model, 'jumlah')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'nama_menu')->widget(Select2::classname(), [
                // 'data' => $data,
                // 'data' => 'sds','asdsad',
                // 'style' => 'color:red',
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                ]); ?>

            <div class="form-group">
                    <label>Minimal</label>
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">Alabama</option>
                      <option>Alaska</option>
                      <option>California</option>
                      <option>Delaware</option>
                      <option>Tennessee</option>
                      <option>Texas</option>
                      <option>Washington</option>
                    </select>
                  </div><!-- /.form-group -->

                <?= $form->field($model, 'sub_harga')->textInput(['maxlength' => true]) ?>
                
                <div class="box" style="border: none;">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success p-100%','style' => 'padding:10px 48%']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                </div>
                
            </div>

    </div>
    <div class="col-md-4 order-md-2 mb-4">
        <div class="col-md-12 mb-3 box box-primary ml-20 ">
            <h1>Total</h1>
                <?php if($name){ ?>
                 <h1><?= $name->nama_pemesan; ?></h1>  
                 <?php }?> 
                 
        </div>
    </div>
</div>





<div class="cc">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Table</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th>#</th>
                            <th>Task</th>
                            <th>Progress</th>
                            <th>label</th>
                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>


