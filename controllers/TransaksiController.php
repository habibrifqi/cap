<?php

namespace app\controllers;

use yii;
use app\models\Barang;
use app\models\Transaksi;
use app\models\TransaksiInfo;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class TransaksiController extends \yii\web\Controller
{
    public function actionIndex()
    {
        
        // $model = new TransaksiInfo();
        // if ($model->load(Yii::$app->request->post())) {
        //     $model->id_tansaksi = 12;
        //     if ($model->validate()) {
        //         // form inputs are valid, do something here

        //             $model->save();

        //         return  $this->redirect(['index', 'id' => $model->id]);
        //     }
        //     }else {
        //     $model->loadDefaultValues();
        //     }
        // return $this->render('index', [
        //     'model' => $model,
        // ]);

        $model = new TransaksiInfo();
        $model1 = new Transaksi();
        $session = Yii::$app->session;
        $session->open();

        if(!$session->has('name')){
            if ($model1->load(Yii::$app->request->post())) {
                $model1->save();
                $session = Yii::$app->session;
                $session['name'] = $model1;

            }
        }else if($session->has('name')){
            if($model->load(Yii::$app->request->post())){
                $name = $session->get('name');
                $model->id_tansaksi = $name->id;
                $model->save();

            }
        }

        // if($model->load(Yii::$app->request->post())){
        //     if($model->validate()){
        //         $session = Yii::$app->session;
        //         $name = $session->get('name');
        //         $model->id_tansaksi = 47;
        //         $model->save();
        //     }
        //     $model1->load(Yii::$app->request->post());
        //     $session = Yii::$app->session;
        //     $session = Yii::$app->session;
        //     $session['name'] = $model1;
        //     $name = $session->get('name');

        // }




        // if ($model1->load(Yii::$app->request->post())) {
        //     // if ($model1->validate()) {
        //     if($model1->save()){
        //         $session = Yii::$app->session;
        //         $session = Yii::$app->session;
        //         $session['name'] = $model1;
        //         $name = $session->get('name');
        //         // echo '<pre>'; print_r($name->id); die;
                
        //     }
        //     // else{
        //     //     echo '<pre>'; print_r($model); die;
        //     // }
        //     return $this->redirect(['index']);
        //     $session = Yii::$app->session;
        //     $name = $session->get('name');
            
        // }else if($model1->save()){
        //     $session = Yii::$app->session;
        //     $name = $session->get('name');
        //     $model->load(Yii::$app->request->post());
        //     $model->id_tansaksi =$name->id;
        //     $model->save();
        // }
        // $cc= "SAS";
        

        return $this->render('index', [
            'model' => $model,'model1' => $model1,
        ]);
    }

    public function actioninput()
    {
        $session = Yii::$app->session;
        $name = $session->get('name');
        $model = new TransaksiInfo();
        $model->load(Yii::$app->request->post());
        $model->id_tansaksi =$name->id;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionTest()
    {
        $session = Yii::$app->session;
        // $name = $session->get('name');
        // $session->close();  // close a session
        $session->remove('name');

        return $this->redirect(['index']);
    }




    protected function findModel($id)
    {
        if (($model = Transaksi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
